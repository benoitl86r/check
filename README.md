<img src="https://gitlab.com/benoitl86r/check/-/raw/master/title.svg" width="100%">

# Check

Premier jet du .bat pour vérifier les postes.
Il apporte pour l'instant une aide jusqu'à la copie des données.

**Prérequis**

Avoir un compte manager admin local et trend micro installé et fonctionnel.


**Utilisation**

Copier le check.bat et le MSERT.exe à la racine d'une clef usb. Faire un clic droit sur le .bat et exécuter en administrateur.

Les infos du poste et du compte utilisateur sont normalement loggués sur la clef usb dans un fichier nomduposte.txt



*Penser à changer le workgroup pour ceux qui ne sont pas en 16 dans les premières lignes*

Si certains connaissent un peu le batch, ils peuvent améliorer ma bouillie.

