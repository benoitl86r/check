@echo off

set WG=CA16
SET mypath=%~dp0
SET mylog=%mypath:~0,-1%/%computername%.txt

rem PSKILL MSERT
rem echo Lancement du scan rapide MSERT en tache de fond... Veuillez patienter quelques minutes...
rem start "" /B %mypath:~0,-1%/MSERT.exe /H /Q
rem pause

cls
:menu
cls
echo Verification du poste
echo ---------------------
echo.
echo.
rem (wmic path Win32_NetworkAdapter where "PNPDeviceID like '%PCI%' AND AdapterTypeID='0'" get name, MacAddress) >  %mylog%
getmac > %mylog%
echo.  >> %mylog%


rem goto :ANTIVIRUS

echo [+] Desactivation de la mise en veille pour manager
powercfg.exe /hibernate off
echo    [OK]

echo [+] Verification que Trend est fonctionnel et a jour.
"C:\Program Files (x86)\Trend Micro\Client Server Security Agent\PccNt.exe"
setlocal
SET /P AREYOUSURE=Trend a jour? On continue (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :reformater
endlocal
echo "Trend vérifié." >> %mylog%

:admbla
echo [+] Verification de la presence du compte %UserDel%
set UserDel=admbla
net user %UserDel% >nul 2>&1


if %ERRORLEVEL% EQU 0 (     
    echo  [!] L'utilisateur %UserDel% existe.
	goto :reformater	
) else (
	echo  [OK] L'utilisateur %UserDel% n'existe pas.
)

echo.
echo [+] Verification de l'existance du dossier utilisateur admbla
IF EXIST C:\users\admbla (
echo  [!] Le dossier utilisateur C:\users\admbla existe.
goto :reformater
) ELSE (
echo  [OK] Pas de dossier utilisateur C:\users\admbla.
)
echo "Compte admbla inexistant." >> %mylog%


:filesintemp
set tempdir="C:\windows\temp\"
echo.
echo [+] Verification de presence d'executables dans %tempdir% :
set fileexe=Y
if exist %tempdir%*.exe ( 
        echo  "[!] Fichiers .exe trouves dans %tempdir% :"
	dir /b /o:n %tempdir%*.exe
	echo "Fichiers trouves dans %tempdir%" >> %mylog%
	dir /b /o:n %tempdir%*.exe >> %mylog%
	set fileexe=N
)

if /I "%fileexe%" equ "Y" (
echo  [OK] Pas d'executable dans %tempdir%
echo "Pas d'executable dans %tempdir%." >> %mylog%
)

setlocal
SET /P AREYOUSURE=Verifier le retour de la presence des exe, on continue (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :reformater
endlocal

:regkeys
echo.
echo [+] Verification des clefs de registre (rundll32 et loader):
set cmd="wmic useraccount get sid"
for /f "skip=1" %%i IN ( ' %cmd% ' ) DO (
	rem echo   [+] Verifier si rundll32 est present :  
	reg query HKEY_USERS\%%i\SOFTWARE\Microsoft\Windows\CurrentVersion\Run 2>&1
	reg query HKEY_USERS\%%i\Software\xsw 2>&1

	)

setlocal
:PROMPT
SET /P AREYOUSURE=Pas de clefs suspicieuses? On continue (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :reformater
endlocal
echo "Registre vérifié." >> %mylog%

echo .
echo "Le planificateur de taches va se lancer. Verifier les taches actives."
rem %windir%\system32\taskschd.msc /s
for /f "tokens=1*" %%a in ('schtasks /query /fo list^|findstr /b "Dossier"') do @echo %%b
setlocal
:PROMPT
SET /P AREYOUSURE=Rien d'anormal dans les taches planifiees? On continue (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :reformater
endlocal
echo "Tâches planifiées vérifiées." >> %mylog%

:ANTIVIRUS 
echo Lancement du scan rapide MSERT en tache de fond... Veuillez patienter quelques minutes... Le fichier de log s'ouvrira qd ca sera fini...
%mypath:~0,-1%/MSERT.exe /H /Q
rem pause


:LOOP
PSLIST MSERT >nul 2>&1
IF ERRORLEVEL 1 (
  GOTO CONTINUE
) ELSE (
  TIMEOUT /T 5
  GOTO LOOP
)

:CONTINUE
echo Check du fichier de log
notepad %SYSTEMROOT%\debug\msert.log

setlocal
SET /P AREYOUSURE=Antivirus fini et OK ? on continue (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :reformater
endlocal
echo "Scan rapide de MSERT ok." >> %mylog%


echo.
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "ATTENTION: Ce poste n'a pas reussi a passer dans le Workgroup %WG%, le faire a la main !"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
goto :MDPMANAGER

:WORKGROUP
echo.
echo [+] Passage au workgroup %WG%
rem wmic /interactive:off ComputerSystem Where name="%computername%" call JoinDomainOrWorkgroup FJoinOptions=3 Name="myDom.local" UserName="myDom\UsrName" Password="@passwrd!@" AccountOU="OU=MyClients;OU=MyOrg;DC=myDom;DC=local"
Wmic /interactive:off computersystem where name="%computername%" call joindomainorworkgroup name=%WG% UserName=".\manager" Password="zyvapas"

setlocal
for /f "usebackq skip=1 tokens=*" %%i in (`wmic computersystem get workgroup ^| findstr /r /v "^$"`) do set WGFound=%%i
echo %WGFound%
  IF %WGFound%==%WG% (
  	GOTO :WGOK
  ) ELSE (
  	GOTO :WGPASOK
  )
endendlocal

:WGPASOK
echo.
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "ATTENTION: Ce poste n'a pas reussi a passer dans le Workgroup %WG%, le faire a la main !"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo. 
pause
goto :OK

:WGOK
echo.
  	echo Poste dans le workgroup %WG%
    echo Poste dans le workgroup %WG% >> %mylog% 

:MDPMANAGER
echo.
echo [+] Changement du mot de passe de manager pour AdM%computername%
net user manager AdM%computername%
echo "Nouveau mdp manager : AdM%computername%" >> %mylog%



:CREATEUSER
@Echo Off
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
set alfanum=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789

set pwd=
FOR /L %%b IN (0, 1, 11) DO (
SET /A rnd_num=!RANDOM! * 62 / 32768 + 1
for /F %%c in ('echo %%alfanum:~!rnd_num!^,1%%') do set pwd=!pwd!%%c
)

SET /p _login=Login de l'utilisateur du poste :
echo login saisi : %_login%
SET /p _password=Password de l'utilisateur (ou Y pour utiliser %pwd%):
IF /I "%_password%" EQU "Y" (set _finishPassword=%pwd%) else (set _finishPassword=%_password%)
echo password choisi : %_finishPassword%
set /p _okuser=Pas d'erreur, on continue (Y/[N])?
IF /I "%_okuser%" NEQ "Y" GOTO :CREATEUSER
echo.
echo [+] Creation de l'utilisateur
net user %_login% %_finishPassword% /ADD
echo "Création du compte %_login% / %_finishPassword%" >> %mylog%

echo [+] Ajout de l'utilisateur dans le groupe Administrateurs
net localgroup Administrateurs %_login% /add
echo "Le compte %_login% est maintenant dans le groupe Administrateurs." >> %mylog%
endlocal

:WORKGROUP2
echo Derniere etape, changement du workgroup et vous pouvez redemarrer quand il vous le propose.
SystemPropertiesComputerName
goto :OK


:reformater
echo.
echo.
echo !!!!!!!!!!!!!!!!!!!!!!!
echo !                     !
echo ! REFORMATER LE POSTE !
echo !                     !
echo !!!!!!!!!!!!!!!!!!!!!!!
pause


:OK
echo.
echo.
echo :-)
