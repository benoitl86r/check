@echo off

SET mypath=%~dp0
SET mylog=%mypath:~0,-1%/%computername%.txt
SET myliste=%mypath:~0,-1%/liste.txt
SET mydroits="Tout le monde:(OI)(CI)F"

rem modification seulement "Tout le monde:(OI)(CI)M"
rem psexec -u MYUSER -p MYPASSWORD MYBATCH.BAT

echo %date% - %time% > %mylog%
echo. >> %mylog%

FOR /F %%i IN (%myliste%) do (
	if exist %%i (
		echo Changement des droits pour %%i 
		takeown /r /d O /f %%i
		icacls %%i /inheritance:e /T /GRANT %mydroits%
		echo OK
		echo Changement des droits pour %%i : OK >> %mylog%
		echo.
	)
)

pause
