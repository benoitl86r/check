# Changement des droits pour les dossiers applicatifs

## Pré-requis
- Un compte manager avec le mdp défini par la précédente procédure
- Le poste est en 64bits, sinon lancer le ChgDroits.bat directement en admin local

## Utilisation
- Copier tous les fichiers sur une clé usb ou dans un répertoire local sans espace ni caractère spécial dans le chemin
- Editer le fichier _liste.txt_ pour ajouter le chemin des dossiers des applicatifs concernés
- Lancer _run.bat_ directement depuis la session de l'utilisateur

