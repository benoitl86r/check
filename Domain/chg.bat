@echo off
goto check_Permissions

:check_Permissions
    echo Administrative permissions required. Detecting permissions...

    net session >nul 2>&1
    if %errorLevel% == 0 (
        GOTO :START
    ) else (
        echo Lancer le script avec les droits administrateur.
    )

    pause >nul
    goto :END

:START
echo Renouvellement des DNS en cours...
ipconfig /renew

SET mypath=%~dp0

powershell Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Bypass && powershell -noninteractive -File %mypath:~0,-1%/domain.ps1

echo Redemarrer sur la session utilisateur si tout est OK au dessus

setlocal
:PROMPT
SET /P AREYOUSURE=Fermer la session utilisateur (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :END
endlocal

logoff 

:END
pause

