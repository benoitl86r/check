@echo off

SET mypath=%~dp0
SET mylog=%mypath:~0,-1%/%USERDOMAIN%-%computername%-%USERNAME%.txt

goto check_Permissions

:check_Permissions
    echo Administrative permissions required. Detecting permissions...

    net session >nul 2>&1
    if %errorLevel% == 0 (
        GOTO :START
    ) else (
        echo Lancer le script avec les droits administrateur.
    )

    pause >nul
    goto :END

:START
echo here we go!



systeminfo > %mylog%

set OLDPROFILE=c:\USERS\%USERNAME%.%computername%

IF EXIST %USERPROFILE% (
	goto :PROFILOLD
) ELSE (
	echo  [!] Pas de dossier du profil du domaine %USERPROFILE%
)
goto :END

:PROFILOLD
IF EXIST %OLDPROFILE% (
	goto :COPYFILES
) ELSE (
	echo  [!] Pas de dossier de l'ancien profil %OLDPROFILE%
)
goto :END
START "" /B explorer %OLDPROFILE%\appdata\roaming

START "" /B explorer %USERPROFILE%\appdata\roaming


:COPYFILES
setlocal
:PROMPT
SET /P AREYOUSURE=Copie de l'ancien profil %OLDPROFILE% vers %USERPROFILE% fini (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :END
endlocal

goto :DROITS

SET RBCOPY=robocopy /copyall /r:1 /w:1 /eta /s /Move
echo copie en cours...

%RBCOPY% %OLDPROFILE%\Desktop %USERPROFILE%\Desktop

%RBCOPY% %OLDPROFILE%\Documents %USERPROFILE%\Documents
%RBCOPY% %OLDPROFILE%\Favorites %USERPROFILE%\Favorites
%RBCOPY% %OLDPROFILE%\Pictures %USERPROFILE%\Pictures
%RBCOPY% %OLDPROFILE%\Music %USERPROFILE%\Music
%RBCOPY% %OLDPROFILE%\Videos %USERPROFILE%\Videos

rem APPDATA\Roaming
%RBCOPY% %OLDPROFILE%\appdata\roaming\Thunderbird %USERPROFILE%\appdata\roaming\Thunderbird
%RBCOPY% %OLDPROFILE%\appdata\roaming\Mozilla %USERPROFILE%\appdata\roaming\Mozilla
%RBCOPY% %OLDPROFILE%\appdata\roaming\Slack %USERPROFILE%\appdata\roaming\Slack
%RBCOPY% %OLDPROFILE%\appdata\roaming\Skype %USERPROFILE%\appdata\roaming\Skype
%RBCOPY% %OLDPROFILE%\appdata\roaming\Adobe %USERPROFILE%\appdata\roaming\Adobe


takeown /r /d O /f %USERPROFILE%\Desktop
takeown /r /d O /f %USERPROFILE%\Documents
takeown /r /d O /f %USERPROFILE%\Favorites
takeown /r /d O /f %USERPROFILE%\Pictures
takeown /r /d O /f %USERPROFILE%\Music
takeown /r /d O /f %USERPROFILE%\Videos
takeown /r /d O /f %USERPROFILE%\appdata\roaming

:DROITS
echo Verification de mozilla
"c:\Program Files\Mozilla Firefox\firefox.exe"

echo Verification de Thunderbird
"c:\Program Files\Mozilla Thunderbird\thunderbird.exe"



setlocal
:PROMPT
SET /P AREYOUSURE=Enlever les droits administrateurs pour %USERDOMAIN%\%USERNAME% (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO :END
endlocal

net localgroup Administrateurs %USERDOMAIN%\%USERNAME% /delete

:END
pause
