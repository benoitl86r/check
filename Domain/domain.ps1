﻿$username = ""
$password = ""

$pwd = ConvertTo-SecureString $password -AsPlainText -Force
$myCred = New-Object System.Management.Automation.PSCredential $username,$pwd

Add-Computer -DomainName CA2475.INTRA -OUPath "OU=COMPUTERS,OU=T2_REG75,OU=ADMIN,DC=CA2475,DC=INTRA" -Credential $myCred
[String] ${stUserDomain},[String]  ${stUserAccount} = $(Get-WMIObject -class Win32_ComputerSystem | select username).username.split("\")

#${stUserAccount}
#[String] ${stUserDomain},[String]  ${stUserAccount} = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name.split("\")

Add-LocalGroupMember Administrateurs -member CA2475\${stUserAccount}

#Write-Host -NoNewLine 'Press any key to continue...';
#$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

