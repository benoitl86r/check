# Passage au nouveau domaine

- Copier les 3 fichier sur une clef usb
- Modifier les 2 premières lignes de domain.ps1 pour ajouter votre compte a2
- Depuis le compte local de l'agent (pas le manager!), faire un clic droit sur chg.bat et l'exécuter en tant qu'administrateur
- Si tout est ok, valider pour changer sur la session CA2475

- Sur la session CA2475 de l'utilisateur, faire un clic droit sur CopyFiles.bat (script pas finalisé, en cours ...) et l'exécuter en tant qu'administrateur
- Suivre les consignes à l'écran
